# O script realiza a busca pelo dia útil anterior com base no arquivo "feriados.csv".
# Feito isso, procura o arquivo.zip com a data correta para extrair os arquivos com final: 
#
# ?
# ?
# ?
# ?
# ?
# 
# e movê-los para seus determinados diretórios.
# Os arquivos que não corresponderem aos sufixos serão excluídos assim como os arquivos.zip 
# com datas diferentes.
#
# O arquivo "feriados.csv" foi disponibilizado no site oficial da Anbima contendo informações sobre
# todos os feriados nacionais até o ano 2078. 
#

import csv 
import os
import sys
import shutil
import zipfile
import time
import logging
import datetime
import calendar
import smtplib
from email.mime.text import MIMEText


# Variáveis a serem parametrizadas
dirCsv 		= 'csv/'
dirLog 		= 'log/'
dirZip 		= 'zip/'
dirExtract 	= 'extract/'
dirFlag 	= 'flag/'
dir1		= 'files1/'
dir2		= 'files2/'

# Função responsável pelo envio de e-mail
def sendMail(text: str):

	# Assunto do e-mail
	SUBJECT = ''

	SMTP_HOST = ''
	SMTP_PORT = 

	USERNAME = ''
	PASSWORD = ''

	FROM 	 = ''
	TO 		 = []

	# Mensagem do e-mail
	MESSAGE 			= MIMEText(text)
	MESSAGE['subject'] 	= SUBJECT
	MESSAGE['from'] 	= FROM
	MESSAGE['to'] 		= ', '.join(TO)
	
	# Tentativa de envio de e-mail
	try:
		
		# Conexão com o servidor
		server = smtplib.SMTP_SSL(SMTP_HOST, SMTP_PORT)

		# Login no servidor
		# server.login(USERNAME, PASSWORD)

		# Enviando e-mail
		server.sendmail(FROM, TO, MESSAGE.as_string())
	
		# Saindo do servidor
		server.quit()

		logger.info("E-mail enviado com sucesso para " + MESSAGE['to'])
		print("E-mail enviado com sucesso para " + MESSAGE['to'])

	except smtplib.SMTPAuthenticationError as e:
		print('Problemas na autententicacao do usuario "{}"' .format(USERNAME))
		logger.error('E-MAIL: Problemas na autententicacao do usuario "{}"' .format(USERNAME))
	except smtplib.SMTPNotSupportedError as e:
		print('Autententicacao nao suportada no servidor SMTP')
		logger.error('E-MAIL: Autententicacao nao suportada no servidor SMTP')
	except smtplib.SMTPException as e:
		print('Problemas ao enviar e-mail para "{}"' .format(MESSAGE['to']))
		logger.error('E-MAIL: Falha ao enviar e-mail para "{}"' .format(MESSAGE['to']))
	except Exception as e:
		# print(e)
		erro(e)


# Função responsável por mover os arquivos corretos para seus determinados diretórios
def moveFile(filename: str):

	diretorio = ''

	try:
		if(filename.endswith("?")):
			diretorio = dir1
			replace(dir1, filename)
		if(filename.endswith("?")):
			diretorio = dir1
			replace(dir1, filename)
		if(filename.endswith("?")):
			diretorio = dir2
			replace(dir2, filename)
		if(filename.endswith("?")):
			diretorio = dir1
			replace(dir1, filename)
		if(filename.endswith("?")):
			diretorio = dir1
			replace(dir1, filename)

		logger.info('MOVE: Arquivo "{}" movido para o diretorio "{}"' .format(filename, diretorio))
	except PermissionError as e:
		logger.error('MOVE: Permissão negada ao mover arquivo "{}" para o diretorio "{}"' .format(arquivo, diretorio))
		sendMail('MOVE: Permissão negada ao mover arquivo "{}" para o diretorio "{}"' .format(arquivo, diretorio))
	except Exception as e:
		# logger.error('Falha ao mover arquivo "{}" para o diretório "{}"' .format(filename, diretorio))
		sendMail('Falha ao executar ação com arquivo "{}"' .format(filename))
		erro(e)

	# print("DIR - DEF: "+diretorio)

# Função responsável por substituir os arquivos já existentes nos diretórios
def replace(diretorio, filename):
	if os.path.isfile(diretorio+filename):
		os.remove(diretorio+filename)
		shutil.move(finalName, diretorio)
		logger.warning('REPLACE: Arquivo "{}" foi substituído no diretório "{}"' .format(filename, diretorio))
	else:
		shutil.move(finalName, diretorio)
		logger.info('MOVE: Arquivo "{}" foi movido para o diretório "{}"' .format(filename, diretorio))

# Função responsável por criar diretórios se não existirem
def createDir(diretorio):
	if(os.path.exists(diretorio)):
		pass
	else:
		logger.info('Criando diretório "{}"' .format(diretorio))
		os.mkdir(diretorio)

def erro(e):
	e = str(e)
	e = e.split(']')
	e = e[1].strip()
	print(e)
	logger.error(e)

# Criação do arquivo .log
DATA_EXEC = time.strftime("%Y")
os.makedirs(os.path.dirname(dirLog), exist_ok=True)
logging.basicConfig(level=logging.INFO,
	format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
	datefmt='%d/%m/%Y - %H:%M:%S',
	filename=dirLog+'rotina_'+ DATA_EXEC +'.log',
	filemode='a')
logger = logging.getLogger(sys.argv[0])

logger.info('Script iniciado')

# criar diretórios se não existirem
# createDir(dirLog)
# createDir(dirExtract)
# createDir(dirFlag)

atual   = datetime.datetime.now()
numWeek = int(atual.strftime('%w'))
dia     = int(atual.strftime('%d'))
mes     = int(atual.strftime('%m'))
ano     = int(atual.strftime('%Y'))

hoje = atual.strftime('%Y%m%d')

file = str(hoje)+'_teste'

count = 0

for _, _, arquivos in os.walk(dirFlag):  
	for arquivo in arquivos:
		count = count + 1

print(count)

aux1 = False
aux2 = False

for _, _, arquivos in os.walk(dirFlag):  
	
	if count != 0:
		
		for arquivo in arquivos:

			print(arquivo)

			if(str(file)+'_ERRO' == str(arquivo)):
				with open(dirFlag+arquivo, 'r', newline = '') as file:
					content = file.readline()
				print(content)
				logger.error('Processo não concluído')
				logger.error(content)
				sendMail(content)
				logger.info('Script finalizado')
				logger.info('------------------------------------------------------------------------------------------------')
				exit(0)
			elif (str(file)+'_OK' == str(arquivo)):
				logger.info('Processo concluído')
				aux1 = True
				# continue
			else:
				logger.error('Flag "{}" possui nome inválido no diretório "{}"' .format(arquivo, dirFlag))
				sendMail('Flag "{}" possui nome inválido no diretório "{}"' .format(arquivo, dirFlag))
				aux2 = True
				# continue
	else:
		logger.error('Nenhuma flag no diretório')
		# try:
		# 	os.remove(dirFlag+arquivo)
		# 	logger.info('DELETE: Flag "{}" excluída' .format(arquivo))
		# except Exception as e:
		# 	pass
		sendMail('Nenhuma flag no diretório "{}"' .format(dirFlag))
		logger.info('Script finalizado')
		logger.info('------------------------------------------------------------------------------------------------')
		exit(0)

if aux1 == False and aux2 == True:
	logger.info('Script finalizado')
	logger.info('------------------------------------------------------------------------------------------------')
	exit(0)

if ((dia == 1) and (mes == 1)):
	mes = 12
	ano = ano-1
	ultimoDiaMes = calendar.monthrange(ano, mes)
	dia = ultimoDiaMes[1]
elif (dia == 1):
	ultimoDiaMes = calendar.monthrange(ano, mes-1)
	dia = ultimoDiaMes[1]
	mes = mes-1
else: 
	dia = dia-1

anterior = datetime.datetime(ano, mes, dia)
numWeek = int(anterior.strftime('%w'))
dia = int(anterior.strftime('%d'))
mes = int(anterior.strftime('%m'))

anterior = int(anterior.strftime('%y%m%d'))

# Consulta arquivo "feriados.csv"
with open(dirCsv+'feriados.csv', 'r', newline = '') as arq:
	arquivo = csv.reader(arq, delimiter = ',')

	for linha in reversed(list(arquivo)):
		diaArq = int(linha[0].split('/')[0])
		mesArq = int(linha[0].split('/')[1])
		anoArq = int(linha[0].split('/')[2])+2000

		feriado = datetime.datetime(anoArq, mesArq, diaArq).strftime('%y%m%d')

		# Percorre os dias anteriores verificando se é útil
		while((numWeek == 0) or (numWeek == 6) or (str(anterior) == feriado)):

			if ((dia == 1) and (mes == 1)):
				mes = 12
				ano = ano-1
				ultimoDiaMes = calendar.monthrange(ano, mes)
				dia = ultimoDiaMes[1]
			elif (dia == 1):
				ultimoDiaMes = calendar.monthrange(ano, mes-1)
				dia = ultimoDiaMes[1]
				mes = mes-1
			else: 
				dia = dia-1

			anterior = datetime.datetime(ano, mes, dia)
			numWeek = int(anterior.strftime('%w'))
			dia     = int(anterior.strftime('%d'))
			anterior = int(anterior.strftime('%y%m%d'))

anteriorD = "D"+str(anterior)

print(anteriorD)

count = 0

for _, _, arquivos in os.walk(dirZip):  
	for arquivo in arquivos:
		count = count + 1

aux = count

# Procura os arquivos no diretório
for _, _, arquivos in os.walk(dirZip):  
	for arquivo in arquivos:
		if((str(arquivo).startswith("COMECO" + str(anteriorD) + ".FIM"))): # Verifica se o arquivo corresponde ao esperado
			data = arquivo.split('.')[3]
			try:
				with zipfile.ZipFile(dirZip+str(arquivo), 'r') as zip_ref:
					zip_ref.extractall(dirExtract) # Realiza a extração de todos os arquivos dentro do .zip
					print(arquivo)
					logger.info('EXTRACT: Arquivo "{}" extraído com sucesso' .format(arquivo))
				aux = aux - 1
				os.remove(dirZip+arquivo) # Após a extração, o arquivo é automaticamente excluído
				logger.info('DELETE: Arquivo "{}" excluído com sucesso' .format(arquivo))
			except Exception as e:
				# logger.error('Arquivo "{}" já existe no diretório "{}"' .format(arquivo, dirExtract))
				erro(e)
		else:
			logger.warning('Arquivo "{}" não preenche os requisitos para ser processado'.format(arquivo))
			try:
				os.remove(dirZip+arquivo) # Os arquivos que não corresponderem a data correta são automaticamente excluídos
				logger.info('DELETE: Arquivo "{}" excluído com sucesso' .format(arquivo))
			except Exception as e:
				print(e)
				# erro(e)

# print(aux)
# print(count)

if aux == count:
	logger.warning('Nenhum arquivo válido para extração')
	sendMail('Nenhum arquivo válido para extração no diretório "{}"' .format(dirZip))

# Faz a busca entre os arquivos extraídos
for extraidos in os.listdir(dirExtract):

	# Verifica o nome dos arquivos
	if(str(extraidos).startswith("COMECO" + str(anterior) + "_XX_" + "FINAL") or
	str(extraidos).startswith("COMECO" + str(anterior) + "_XX_" + "FINAL") or
	str(extraidos).startswith("COMECO" + str(anterior) + "_XX_" + "FINAL") or
	str(extraidos).startswith("COMECO" + str(anterior) + "_XX_" + "FINAL") or
	str(extraidos).startswith("COMECO" + str(anterior) + "_XX_" + "FINAL")
	):
		print(str(extraidos))
		inicialName = dirExtract + str(extraidos)
		finalName = str(extraidos)
		finalName = finalName.replace("COMECO", "11111_")
		finalName = finalName.replace("_XX_", "_")
		finalName = finalName.replace(".XYZ", ".XYZ")
		filename  = finalName
		finalName = dirExtract + finalName
        
		try:
			os.rename(inicialName, finalName) # Renomeia os arquivos para seus determinados nomes 
		except Exception as e:
			logger.error('RENAME: Arquivo "{}" já existe no diretório "{}"' .format(str(extraidos), dirExtract))
		
		moveFile(filename) # Chama a função que irá verificar e mover os arquivos
		
	else:
		os.remove(dirExtract + str(extraidos)) # Os arquivos que não corresponderem serão excluídos

logger.info('Script finalizado')
logger.info('------------------------------------------------------------------------------------------------')